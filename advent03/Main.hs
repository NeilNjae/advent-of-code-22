-- Writeup at https://work.njae.me.uk/2022/12/03/advent-of-code-2022-day-3/

import AoC
import Data.Char
import qualified Data.Set as S
import Data.List
import Data.List.Split

type Contents = S.Set Char
data Rucksack = Rucksack String String deriving (Show, Eq)

main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      text <- readFile dataFileName
      let rucksacks = fmap mkRucksack $ lines text
      print $ part1 rucksacks
      print $ part2 rucksacks

part1 :: [Rucksack] -> Int
part1 = sum . fmap (priority . commonItem)

part2 :: [Rucksack] -> Int
part2 rucksacks = sum $ fmap priority badges
  where groups = chunksOf 3 rucksacks
        badges = fmap badgeOf groups

merge :: Rucksack -> Contents
merge (Rucksack contents1 contents2) = 
  S.union (S.fromList contents1) (S.fromList contents2)

badgeOf :: [Rucksack] -> Char
badgeOf rucksacks = S.findMin $ intersections (fmap merge rucksacks)

intersections :: [Contents] -> Contents
intersections sets = foldl1' S.intersection sets


mkRucksack :: String -> Rucksack
mkRucksack contents = Rucksack (take n contents) (drop n contents)
  where n = length contents `div` 2

commonItem :: Rucksack -> Char
commonItem (Rucksack contents1 contents2) = S.findMin (c1 `S.intersection` c2)
  where c1 = S.fromList contents1
        c2 = S.fromList contents2

priority :: Char -> Int
priority item 
  | isLower item = ord item - ord 'a' + 1
  | otherwise = ord item - ord 'A' + 27
