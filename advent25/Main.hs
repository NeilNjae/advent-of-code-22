-- Writeup at https://work.njae.me.uk/2022/12/01/advent-of-code-2022-day-1/

import AoC
import Data.List


main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      numStrs <- readFile dataFileName
      let fuels = fmap readSnafu $ lines numStrs
      putStrLn $ showSnafu $ sum fuels

readSnafu :: String -> Int
readSnafu cs = foldl' go 0 cs
  where go acc c = acc * 5 + (snafuValue c)

snafuValue :: Char -> Int
snafuValue '2' = 2
snafuValue '1' = 1
snafuValue '0' = 0
snafuValue '-' = -1
snafuValue '=' = -2
snafuValue _ = error "Illegal digit in read"

showSnafu :: Int -> String
showSnafu = packSnafu . toBase5R

toBase5R :: Int -> [Int]
toBase5R 0 = []
toBase5R n = (r : (toBase5R k))
  where (k, r) = n `divMod` 5

packSnafu :: [Int] -> String
packSnafu digits
  | carry == 0 = shown
  | otherwise = (snafuRep carry) : shown
  where (carry, shown) = foldl' packSnafuDigit (0, "") digits

packSnafuDigit :: (Int, String) -> Int -> (Int, String)
packSnafuDigit (carry, acc) d 
  | d' <= 2 = (0, (snafuRep d') : acc)
  | otherwise = (1, (snafuRep (d' - 5) : acc))
  where d' = d + carry

snafuRep :: Int -> Char
snafuRep 2 = '2'
snafuRep 1 = '1'
snafuRep 0 = '0'
snafuRep -1 = '-'
snafuRep -2 = '='
snafuRep _ = error "Illegal number in show"
