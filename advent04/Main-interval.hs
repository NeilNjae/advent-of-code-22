-- Writeup at https://work.njae.me.uk/2022/12/04/advent-of-code-2022-day-4/

import AoC
import Data.Text (Text)
import qualified Data.Text.IO as TIO
import Data.Attoparsec.Text hiding (Result)
import Numeric.Interval hiding (null)
-- import Control.Applicative

type Assignment = Interval Int
type Pair = (Assignment, Assignment)

main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      text <- TIO.readFile dataFileName
      let pairs = successfulParse text
      print $ part1 pairs
      print $ part2 pairs

part1 :: [Pair] -> Int
part1 = length . (filter hasContainment)

part2 :: [Pair] -> Int
part2 = length . (filter $ uncurry (==?))

hasContainment :: Pair -> Bool
hasContainment (assignment1, assignment2) = 
  (assignment1 `contains` assignment2) || (assignment2 `contains` assignment1)


-- Parse the input file

pairsP :: Parser [Pair]
pairP :: Parser Pair
assignmentP :: Parser Assignment

pairsP = pairP `sepBy` endOfLine
pairP = (,) <$> assignmentP <* "," <*> assignmentP

assignmentP = (...) <$> decimal <* "-" <*> decimal

successfulParse :: Text -> [Pair]
successfulParse input = 
  case parseOnly pairsP input of
    Left  _err -> [] -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right pairs -> pairs
