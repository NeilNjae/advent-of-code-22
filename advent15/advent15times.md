| Program                  | &nbsp;System time&nbsp; | &nbsp;User time&nbsp; | &nbsp;Wall time&nbsp; | &nbsp;Memory used (kb)&nbsp; |
|:-------------------------|------------:|----------:|----------:|------------:|
| Original                 |       18.47 |    202.19 |   2:02.91 |  17,888,168 |
| Sorted region            |       18.09 |    189.46 |   2:00.68 |  17,886,248 |
| Lazy boundary creation   |        0.10 |      5.69 |   0:05.64 |      80,708 |
| Direct parallelism       |        3.34 |     27.14 |   0:11.49 |   1,322,200 |
| Parallelism and chunking |        0.59 |     14.82 |   0:02.36 |     672,592 |
