-- Writeup at https://work.njae.me.uk/2022/12/01/advent-of-code-2022-day-1/

import AoC
import Data.List
import Data.List.Split
import Data.Ord

main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      numStrs <- readFile dataFileName
      let calories = fmap (fmap (read @Int)) $ splitWhen null $ lines numStrs
      print $ part1 calories
      print $ part2 calories

part1 :: [[Int]] -> Int
part1 = maximum . fmap sum

part2 :: [[Int]] -> Int
-- part2 = sum . take 3 . reverse . sort . fmap sum
part2 = sum . take 3 . sortOn Down . fmap sum

